$(document).ready(function(){
    console.log('ready');


    const wholeForm = $('#login-form');
    const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    let isShow = true;
    const inputPassword = $('#pass-input');
    const buttonHideShow = $('#button-hide-show');
    const inputHelper = $('#input-helper');


    buttonHideShow.on('click', handleHideShow);
    inputPassword.on('keyup', handlePasswordStrength);
    wholeForm.submit(handleSubmit);
    

    function handleHideShow(e) {
        if (isShow) {
          inputPassword.attr('type', 'text');
          isShow = false;
          buttonHideShow.html('hide');
        } else {
          inputPassword.attr('type', 'password');
          isShow = true;
          buttonHideShow.html('show');
        }
      }
    
    function handleSubmit(e){
        e.preventDefault();
        const username = $('#uname-input').val();
        const pass = $('#pass-input').val();
        $('.error').remove();

        if(username.length<1){
            $('#uname-input').after('<div class="error">Username cannot be empty</div>');
        } else if(username.length<8){
            $('#uname-input').after('<div class="error">Must be at least 8 characters</div>');
        }

        if(pass.length<1){
            $('#input-helper').after('<div class="error">Password cannot be empty</div>');
        } else if(pass.length<8){
            $('#input-helper').after('<div class="error">Must be at least 8 characters</div>');
        }
    };

    function handlePasswordStrength() {
        const value = $(this).val();
        if(value===''){
            inputHelper.html('');
        } else if (strongRegex.test(value)) {
          console.log('password strong');
          inputHelper.html('Strong').addClass('password-strong').removeClass('password-medium password-weak');
        } else if (mediumRegex.test(value)) {
          console.log('password medium');
          inputHelper.html('Medium').addClass('password-medium').removeClass('password-strong password-weak');
        } else {
          console.log('password weak');
          inputHelper.html('Weak').addClass('password-weak').removeClass('password-medium password-strong');
        }
      }
    
    
});